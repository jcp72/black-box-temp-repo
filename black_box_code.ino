/***************************
 * DUKE AERO BLACK BOX CODE
 * 5 APRIL 2024
 * JONATHAN PILAND
 * jpiland16@gmail.com
 ***************************/

/***
 * 
 * General operation:
 * 
 *  - Read data from Serial 1 (Flight controller) and write directly to a text file.
 *  - Read compressed data from Serial 2 (Ellipse) and parse it to convert to a usable format, 
 *      then write to a similarly-named (but different) file.
 * 
 * Concerns (last updated Fri 5 Apr 2024, 4:10am EDT, by Jonathan Piland)
 * 
 *  - May need to slow Ellipse data down, as it appears flash writes can cause us to miss data.
 *  - This may also be a concern for the other data stream (serial 1).
 *  - Need to test FC serial.
 * 
*/

#include <Arduino.h> 
#include <EEPROM.h>
#include <LittleFS.h>
#include "sbg.hpp"

/* THINGS THAT SHOULD BE SET TO FALSE BEFORE FLIGHT */
#define TEST_MODE false
#define FILE_RETRIEVAL_MODE false

#define USE_SERIAL (TEST_MODE || FILE_RETRIEVAL_MODE) // Serial is required for either of these


// TODO: Change these to the correct ports!
#define FC_SERIAL Serial1
#define EL_SERIAL Serial2

int frameCounter = 0;
int droppedFrameCounter = 0;
long bytesReceivedCounter = 0;

/* CONSTANTS */
#define BUZZER_PIN 41

String fc_file_name;
String el_file_name;
LittleFS_QSPIFlash myfs;

SbgStandardFrame* framePtr;
SystemData* systemDataPtr;

void beep(int durationMilliseconds, int frequency) {
    noTone(BUZZER_PIN);
    tone(BUZZER_PIN, frequency);
    delay(durationMilliseconds);
    noTone(BUZZER_PIN);
}

void printFiles() {
    File root = myfs.open("/");
    File entry;
    while (entry = root.openNextFile()) {
        if (!entry.isDirectory() ){//&& entry.name()[0] == 'B') {
            // print file name and size
            Serial.print(entry.name());
            Serial.print("  (");
            Serial.print(entry.size(), DEC);
            Serial.println(" B)");
        }
    }
}

void setup() {

    if (USE_SERIAL) Serial.begin(115200);

    FC_SERIAL.begin(115200);
    EL_SERIAL.begin(115200);

    framePtr = (SbgStandardFrame*) malloc(sizeof(SbgStandardFrame));
    systemDataPtr = (SystemData*) malloc(sizeof(SystemData));

    int boot_number = (unsigned int)EEPROM.read(0)+1;
    EEPROM.write(0, boot_number);

    fc_file_name = "BB_Flight_Log_" + String(boot_number) + ".txt";
    el_file_name = "BB_Ellipse_Log_" + String(boot_number) + ".txt";

    if (USE_SERIAL) Serial.println(fc_file_name);
    if (USE_SERIAL) Serial.println(el_file_name);

    // Set up buzzer
    pinMode(BUZZER_PIN, OUTPUT);

    // Initialize LittleFS
    if (!myfs.begin()) {
        if (USE_SERIAL) Serial.printf("Error initializing QSPI FLASH");
        tone(BUZZER_PIN, 660);
        while (1);
    }
    if (USE_SERIAL) Serial.println("LittleFS initialized");

    

    if (TEST_MODE || FILE_RETRIEVAL_MODE) {
        // WARN by using the buzzer, this condition should not be true for flight
        // If in regular testing mode, the buzzer will beep 10 "A"s (all the same note)
        // If in file retrieval mode, the buzzer will alternate between E and A (high and low notes).
        for (int i = 0; i < 5; i++) {
            beep(100, (FILE_RETRIEVAL_MODE) ? 660 : 440);
            delay(100);
            beep(100, 440);
            delay(100);
        }
    } else {
        // NOTIFY using quick beeps that configuration is OK for flight.
        for (int i = 0; i < 3; i++) {
            beep(50, 440);
            delay(50);
            beep(50, 550);
            delay(50);
            beep(50, 660);
            delay(50);
        }
    }

    if (FILE_RETRIEVAL_MODE) {
        // NOTE: This overrides "test mode", and keeps the code in a loop below.

        // First, print all files in the root directory
        // Serial.println("Root files starting with 'B':");
        Serial.println("Root files:");

        printFiles();

        while (true) {
            Serial.println("Waiting for file name entry (<64 chars) to echo...");
            int bufferPos = 0;
            char buffer[64] = { 0 };
            while (bufferPos <= 64) {
                if (Serial.available()) {
                    char c = Serial.read();
                    if (c == '\n') break;
                    buffer[bufferPos] = c;
                    bufferPos++;
                }
            }
            if (bufferPos == 64) {
                Serial.println("Buffer overflow. Clearing...");
            } else {
                // There was a file name, plus the ENTER character

                File logFile = myfs.open(buffer);
                if (logFile) {
                    // Serial.printf("Reading %s to serial...\n", buffer);
                    while (logFile.available()) {
                        Serial.write(logFile.read());
                    }
                    logFile.close();
                    Serial.println();
                } else {
                    Serial.printf("Error opening %s\n", buffer);
                    printFiles();
                }  
            }
        }
    }

}

void loop() {

    // READ ELLIPSE DATA
    while (EL_SERIAL.available()) {

        byte myByte = EL_SERIAL.read();

        int result = processSbgByte(myByte, framePtr);
        bytesReceivedCounter += 1;

        if (result == SBG_PARSE_DONE) {
            frameCounter++;
            if (!frameCRCIsValid(framePtr)) {
                droppedFrameCounter++;
            } else {
                // Successful frame parsing
                if (framePtr->messageClass != 0) {
                } else {
                    retrieveBinaryLogInfo(framePtr, systemDataPtr);
                    
                    // Example application: write to the Ellipse log file any time we receive IMU data
                    if (framePtr->messageId == SBG_ECOM_LOG_IMU_DATA) {
                        File logFile = myfs.open(el_file_name.c_str(), FILE_WRITE);
                        if (logFile) {
                            sbgEComLogIMUData imu = systemDataPtr->IMUData;
                            logFile.printf("IMU: %f %f %f\n", imu.accelX_meters_per_s_2, imu.accelY_meters_per_s_2, imu.accelZ_meters_per_s_2);
                            logFile.close();
                            if (USE_SERIAL) Serial.println("IMU data written!");
                        } else {
                            if (USE_SERIAL) Serial.println("Error opening log file!");
                        }
                    }

                }
            }
        }
    }

    // WARNING: UNTESTED CODE FOLLOWS - this will almost CERTAINLY need to be modified, especially with ellipse timing constraints!

    File logFile = myfs.open(fc_file_name.c_str(), FILE_WRITE);
    if (logFile) {
        while (FC_SERIAL.available()) {
            logFile.write(FC_SERIAL.read());
        }
        logFile.close();
    } else {
        if (USE_SERIAL) Serial.println("Error opening log file!");
    }

}
