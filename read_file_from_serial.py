import serial, re, os
from datetime import datetime

FC_MATCH_RE = re.compile(r"BB_Flight_Log_(\d+)\.txt")
EL_MATCH_RE = re.compile(r"BB_Ellipse_Log_(\d+)\.txt")

PORT = "/dev/ttyACM0"

def main():
    teensy = serial.Serial(port=PORT, baudrate=115200, timeout=1) 

    print("Looking for files...")
    teensy.write(b"test.txt\n") # dummy so we can retrieve all file names

    buffer = ""

    while True:
        char = teensy.read(1)
        if len(char) == 0:
            break
        buffer += char.decode()

    # print(buffer)

    # Buffer contains all file names
        
    max_file_number = -1
        
    for line in buffer.splitlines():
        fc_match = FC_MATCH_RE.search(line)
        el_match = EL_MATCH_RE.search(line)

        if fc_match:
            number = int(fc_match.groups()[0])
            if number > max_file_number:
                max_file_number = number
        if el_match:
            number = int(el_match.groups()[0])
            if number > max_file_number:
                max_file_number = number

    # print(f"Max file number is {max_file_number}")
                
    if not os.path.exists("downloads"):
        os.mkdir("downloads")
    
    today_date = "{:%Y%m%d_%H%M%S}".format(datetime.now())

    # os.mkdir(os.path.join("downloads", today_date)) ; read_file(teensy, "dataLog97.txt", today_date)

    if max_file_number > -1:
        os.mkdir(os.path.join("downloads", today_date))
        read_file(teensy, f"BB_Flight_Log_{max_file_number}.txt", today_date)
        read_file(teensy, f"BB_Ellipse_Log_{max_file_number}.txt", today_date)
    else:
        print("No relevant files found!")

def read_file(device, file_name, date_string):
    print(f"Reading file {file_name}...")
    device.write((file_name + "\n").encode())
    buffer = ""
    while True:
        char = device.read(1)
        if len(char) == 0:
            break
        buffer += char.decode()

    buffer = buffer.replace("Waiting for file name entry (<64 chars) to echo...", "")

    with open(os.path.join("downloads", date_string, file_name), "w") as file:
        file.write(buffer)

if __name__ == "__main__":
    try:
        main()
    except serial.SerialException:
        print("MAKE SURE TO CLOSE ARDUINO SERIAL MONITOR!!!")
        raise
