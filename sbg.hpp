#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define HAS_8_BYTE_FLOATS true

#if HAS_8_BYTE_FLOATS
typedef double _double;
#else
typedef struct __double {
    char bytes[8]; 
} _double;

float doubleToFloat(_double myDouble) {
    // 64-bit double: 1-bit sign + 11-bit exponent + 52-bit mantissa
    // 32-bit double: 1-bit sign +  8-bit exponent + 23-bit mantissa
    /*
     > 64-bit
        | 7               | 6               | 5               | 4               | 3               | 2               | 1               | 0               | 
        |-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|
        | S E E E E E E E | E E E E M M M M | M M M M M M M M | M M M M M M M M | M M M M M M M M | M M M M M M M M | M M M M M M M M | M M M M M M M M |
        |-----------------|---------*-*-*-*-|-*-*-*-_-_-_-_-_-|-_-_-_-----------|-----------------|-----------------|-----------------|-----------------|
        | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 |
     > 32-bit
        | 3               | 2               | 1               | 0               |
        |-----------------|-----------------|-----------------|-----------------|
        | S E E E E E E E | E M M M M M M M | M M M M M M M M | M M M M M M M M |
        |-----------------|-----------------|-----------------|-----------------|
        | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 | 7 6 5 4 3 2 1 0 |
    */
    char returnBytes[4];

    int exponent = ((myDouble.bytes[6] & 0b11110000) >> 4) + ((myDouble.bytes[7] & 0b01111111) << 4) - 1023 + 127;

    // Check sign bit on exponent (is this really important if we are chopping off the left side of it anyway??)
    // if (exponent >> 9) {
    //      exponent = exponent | 0xFFFF8000; // Add all the sign bits
    // }
    
    // Calculate mantissa
    returnBytes[2]  = ((myDouble.bytes[6] & 0b00001111) << 3) + ((myDouble.bytes[5] & 0b11100000) >> 5);
    returnBytes[1]  = ((myDouble.bytes[5] & 0b00011111) << 3) + ((myDouble.bytes[4] & 0b11100000) >> 5);
    returnBytes[0]  = ((myDouble.bytes[4] & 0b00011111) << 3) + ((myDouble.bytes[3] & 0b11100000) >> 5);

    // Insert exponent
    returnBytes[2] = returnBytes[2] | ((exponent & 1) << 7);
    returnBytes[3] = (exponent >> 1) & 0b01111111;

    // Insert sign
    returnBytes[0] = returnBytes[0] | (myDouble.bytes[0] & 0b10000000);

    float returnFloat;
    memcpy(&returnFloat, returnBytes, 4);

    // for (int i = 0; i < 4; i++) {
    //     printf("%d: " BYTE_TO_BINARY_PATTERN "\n", i, BYTE_TO_BINARY(returnBytes[i]));
    // }

    return returnFloat;
}
#endif

typedef struct __attribute__ ((__packed__)) {
    uint8_t messageId;
    uint8_t messageClass;
    uint16_t dataLength;
    uint8_t data[4096];
    uint16_t crc;
} SbgStandardFrame;

enum SBG_PROCESS_RESULT {
    SBG_PARSE_IN_PROGRESS,
    SBG_PARSE_DONE
};

int processSbgByte(unsigned char, SbgStandardFrame*);
void printBinaryLogInfo(SbgStandardFrame* frame);
bool frameCRCIsValid(SbgStandardFrame*);

// from sbgEComIds.h
typedef enum _SbgEComLog
{
    SBG_ECOM_LOG_STATUS    = 1,  /*!< Status general, clock, com aiding, solution, heave */
    SBG_ECOM_LOG_UTC_TIME  = 2,  /*!< Provides UTC time reference */
    SBG_ECOM_LOG_IMU_DATA  = 3,  /*!< Includes IMU status, acc., gyro, temp delta speeds and delta angles values */
    SBG_ECOM_LOG_MAG       = 4,  /*!< Magnetic data with associated accelerometer on each axis */
    SBG_ECOM_LOG_EKF_EULER = 6,  /*!< RPY + accuracy */
    SBG_ECOM_LOG_EKF_QUAT  = 7,  /*!< Includes the 4 quaternions values */
    SBG_ECOM_LOG_EKF_NAV   = 8,  /*!< Position and velocities in NED coordinates with the accuracies on each axis */
    SBG_ECOM_LOG_GPS1_VEL  = 13, /*!< GPS velocities from primary or secondary GPS receiver */
    SBG_ECOM_LOG_GPS1_POS  = 14, /*!< GPS positions from primary or secondary GPS receiver */
    SBG_ECOM_LOG_AIR_DATA  = 36, /*!< Air Data aiding such as barometric altimeter and true air speed. */
} SbgEComLog;

// WARNING: unaligned integers. Not sure if this is ideal or not.
// This applies to all following structs.

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    uint16_t generalStatus;
    uint16_t reserved1;
    uint32_t comStatus;
    uint32_t aidingStatus;
    uint32_t reserved2;
    uint16_t reserved3;
    uint32_t upTimeSeconds;
} sbgEComLogStatus;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    uint16_t clockStatus;
    uint16_t year;
    uint8_t  month;
    uint8_t  day;
    uint8_t  hour;
    uint8_t  minute;
    uint8_t  second;
    uint32_t nanosecond;
    uint32_t gpsTOWMilliseconds;
} sbgEComLogUTCTime;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    uint16_t imuStatus;
    float    accelX_meters_per_s_2;
    float    accelY_meters_per_s_2;
    float    accelZ_meters_per_s_2;
    float    gyroX_radians_per_s;
    float    gyroY_radians_per_s;
    float    gyroZ_radians_per_s;
    float    temperatureDegreesC;
    float    deltaVelX_meters_per_s_2;
    float    deltaVelY_meters_per_s_2;
    float    deltaVelZ_meters_per_s_2;
    float    deltaAngleX_radians_per_s;
    float    deltaAngleY_radians_per_s;
    float    deltaAngleZ_radians_per_s;
} sbgEComLogIMUData;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    uint16_t magStatus;
    float    magX;
    float    magY;
    float    magZ;
    float    accelX_meters_per_s_2;
    float    accelY_meters_per_s_2;
    float    accelZ_meters_per_s_2;
} sbgEComLogMag;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    float    roll;
    float    pitch;
    float    yaw;
    float    roll_acc_rad;
    float    pitch_acc_rad;
    float    yaw_acc_rad;
    uint32_t solutionStatus;
} sbgEComLogEKFEuler;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    float    q0;
    float    q1;
    float    q2;
    float    q3;
    float    roll_acc_rad;
    float    pitch_acc_rad;
    float    yaw_acc_rad;
    uint32_t solutionStatus;
} sbgEComLogEKFQuat;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    float    velocityN_meters_per_s;
    float    velocityE_meters_per_s;
    float    velocityD_meters_per_s;
    float    velocityN_acc_meters_per_s;
    float    velocityE_acc_meters_per_s;
    float    velocityD_acc_meters_per_s;
    _double  latitude_degrees;
    _double  longitude_degrees;
    _double  altitude_meters;
    float    undulation_meters;
    float    latitude_acc_meters;
    float    longitude_acc_meters;
    float    altitude_acc_meters;
    uint32_t solutionStatus;
} sbgEComLogEKFNav;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    uint32_t gpsVelStatus;
    uint32_t gpsTOWMilliseconds;
    float    velocityN_meters_per_s;
    float    velocityE_meters_per_s;
    float    velocityD_meters_per_s;
    float    velocityN_acc_meters_per_s;
    float    velocityE_acc_meters_per_s;
    float    velocityD_acc_meters_per_s;
    float    course_degrees;
    float    course_acc;
} sbgEComLogGPS1Vel;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampMicroseconds;
    uint32_t gpsPosStatus;
    uint32_t gpsTOWMilliseconds;
    _double  latitude_degrees;
    _double  longitude_degrees;
    _double  altitude_meters;
    float    undulation_meters;
    float    latitude_acc_meters;
    float    longitude_acc_meters;
    float    altitude_acc_meters;
    uint8_t  numSVUsed;
    uint16_t baseStationID;
    uint16_t differentialDataAge_hundredths_of_s;
} sbgEComLogGPS1Pos;

typedef struct __attribute__ ((__packed__)) {
    uint32_t timestampOrMeasurementDelayMicroseconds;
    uint16_t airdataStatus;
    float    absoluteBarometerPressure_Pascals;
    float    barometerAltitude_meters;
    float    pitotTubeDifferentialPressure_Pascals;
    float    pitotTubeTrueAirspeed_meters_per_s;
    float    airTemperature_degreesC;
} sbgEComLogAirData;

typedef struct {
    sbgEComLogStatus   status;
    sbgEComLogUTCTime  UTCTime;
    sbgEComLogIMUData  IMUData;
    sbgEComLogMag      mag;
    sbgEComLogEKFEuler EKFEuler;
    sbgEComLogEKFQuat  EKFQuat;
    sbgEComLogEKFNav   EKFNav;
    sbgEComLogGPS1Vel  GPS1Vel;
    sbgEComLogGPS1Pos  GPS1Pos;
    sbgEComLogAirData  airData;
} SystemData;
