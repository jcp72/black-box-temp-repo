#include <stdio.h>
#include <string.h>

// PAGE 70: Binary Output Logs Overview

void retrieveBinaryLogInfo(SbgStandardFrame* frame, SystemData* systemData) {
    // For messages with Class 0x00 only

    uint8_t messageId = frame->messageId;

    switch(messageId) {
        case SBG_ECOM_LOG_STATUS:
            // This output combines all system status data, divided into several categories: General, Communications,
            // Aiding. 
            // This log is useful for advanced status information.
            if (frame->dataLength != 26) Serial.println("Invalid data length!");
            if (sizeof(sbgEComLogStatus) != 26) Serial.println("Invalid structure size!");
            memcpy(&(systemData->status), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_UTC_TIME:
            // Provides UTC time reference. This frame also provides a time correspondence between the device
            // TIME_STAMP value and the actual UTC Time. You thus have to use this frame if you would like to time
            // stamp all data to an absolute UTC or GPS time reference.
            if (frame->dataLength != 21) Serial.println("Invalid data length!");
            if (sizeof(sbgEComLogUTCTime) != 21) Serial.println("Invalid structure size!");
            memcpy(&(systemData->UTCTime), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_IMU_DATA:
            // Provides accelerometers, gyros, delta angles and delta velocities data directly from the IMU.
            if (frame->dataLength != 58) Serial.println("Invalid data length!");
            if (sizeof(sbgEComLogIMUData) != 58) Serial.println("Invalid structure size!");
            memcpy(&(systemData->IMUData), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_MAG:
            // Provides magnetometer data and associated accelerometer. In case of internal magnetometer used, the
            // internal accelerometer is also provided
            if (frame->dataLength != 30) Serial.println("Invalid mag data length!");
            if (sizeof(sbgEComLogMag) != 30) Serial.println("Invalid mag structure size!");
            memcpy(&(systemData->mag), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_EKF_EULER:
            // Provides orientation in RPY format.
            if (frame->dataLength != 32) Serial.println("Invalid euler data length!");
            if (sizeof(sbgEComLogEKFEuler) != 32) Serial.println("Invalid euler structure size!");
            memcpy(&(systemData->EKFEuler), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_EKF_QUAT:
            // Provides orientation in quaternion format.
            if (frame->dataLength != 36) Serial.println("Invalid quat data length!");
            if (sizeof(sbgEComLogEKFQuat) != 36) Serial.println("Invalid quat structure size!");
            memcpy(&(systemData->EKFQuat), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_EKF_NAV:
            // Provides velocity in NED coordinate system and position (Latitude, Longitude, Altitude), and associated
            // accuracy parameters.
            if (frame->dataLength != 72) Serial.println("Invalid nav data length!");
            if (sizeof(sbgEComLogEKFNav) != 72) Serial.println("Invalid nav structure size!");
            memcpy(&(systemData->EKFNav), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_GPS1_VEL:
            // Provides velocity and course information from primary or secondary GNSS receiver. The time stamp is not
            // aligned on main loop but instead of that, it dates the actual GNSS velocity data.
            if (frame->dataLength != 44) Serial.println("Invalid vel data length!");
            if (sizeof(sbgEComLogGPS1Vel) != 44) Serial.println("Invalid vel structure size!");
            memcpy(&(systemData->GPS1Vel), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_GPS1_POS:
            // Provides position information from primary or secondary GNSS receiver.
            // The time stamp is not aligned on main loop but instead of that, it dates the actual GPS position data.
            if (frame->dataLength != 57) Serial.println("Invalid gps1 data length!");
            if (sizeof(sbgEComLogGPS1Pos) != 57) Serial.println("Invalid gps1 structure size!");
            memcpy(&(systemData->GPS1Pos), frame->data, frame->dataLength);
            break;
        case SBG_ECOM_LOG_AIR_DATA:
            // The Airdata log provides both a barometric altitude above reference level as well as a true airspeed
            // indication. The altitude is generally measured using a barometer whereas the true airspeed is measured
            // using a pitot tube. The altitude is by default referenced to a standard 1013.25 hPa zero level pressure.
            // Unlike other sbgECom logs, this log is both used as an input and output log. It is send by the unit when a
            // new internal airdata information is available but it can also be used to inject an external altitude and true
            // airspeed aiding information
            if (frame->dataLength != 26) Serial.println("Invalid air data length!");
            if (sizeof(sbgEComLogAirData) != 26) Serial.println("Invalid air structure size!");
            memcpy(&(systemData->airData), frame->data, frame->dataLength);
            break;
        default:
            Serial.print("Processing of message ID ");
            Serial.print(messageId);
            Serial.print(" not yet implemented.\n");
    }

}
