#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

enum STATE {
    INITIAL,
    MAGIC_WORD_1_SEEN,
    MAGIC_WORD_2_SEEN,
    MESSAGE_ID_SEEN,
    CLASS_SEEN,
    DATA_LENGTH_SEEN,
    DATA_DONE,
    CRC_SEEN,
    ETX_SEEN
};

// #define DEBUG_PRINT false
#define DEBUG_PRINT true

uint16_t calcCRC(const void*, uint16_t);

bool frameCRCIsValid(SbgStandardFrame* frame) {

    uint16_t crcInputLength = frame->dataLength + 4;


    uint16_t calculatedCRC = calcCRC((const void*) frame, crcInputLength);

    // Serial.println(frame->crc);
    // Serial.println(calculatedCRC);
    // Serial.println(frame->dataLength);

    return (frame->crc == calculatedCRC);    
}

// From SBG manual
uint16_t calcCRC(const void *pBuffer, uint16_t bufferSize) {
    const uint8_t* pBytesArray = (const uint8_t*) pBuffer;
    uint16_t poly = 0x8408;
    uint16_t crc = 0;
    uint8_t carry;
    uint8_t i;
    uint16_t j;
    assert(pBuffer);
    
    for (j = 0; j < bufferSize; j++) {
        // Serial.print(j);
        // Serial.print(": ");
        // Serial.println(crc);
        crc = crc ^ ((uint16_t) pBytesArray[j]);
        // Serial.print(j);
        // Serial.print(" (byte ");
        // Serial.print(pBytesArray[j]);
        // Serial.print("): ");
        // Serial.println(crc);
        for (i = 0; i < 8; i++) {
            carry = crc & 1;
            crc = crc / 2;
            if (carry) {
                crc = crc ^ poly;
            }
        }
    }   
    return crc;
}

int processSbgByte(uint8_t myByte, SbgStandardFrame* frame) {

    // TODO: handle seeing the magic word when it's not actually the start of a
    // frame (This is not likely, 1/(2^16) = 0.0015% chance PER BYTE in average 
    // frame size. So for 50-byte frame, we expect to hear 25 bytes beforehand
    // which means 1 - (1 - 0.000015)^25 = 0.03% chance overall)

    // static uint8_t  buffer[4096]; // to be used in handling above todo
    // static uint16_t bufferPos; // same here

    static bool     initialization = true; // We have never seen the correct 2-magic-word combo.
    static uint8_t  state = INITIAL;
    static uint32_t ignoreCount = 0;
    static uint8_t  dataLengthPosition = 0; // Needed because 2 bytes
    static uint16_t dataPosition = 0; // Needed because data could be 0 - 4086 bytes
    static uint8_t  crcPosition = 0; // Needed because 2 bytes

    // For teensy
    static uint16_t frameDataLength = 0;
    static uint16_t frameCRC = 0;

    switch(state) {

        case INITIAL:
            if (myByte == 0xFF) state = MAGIC_WORD_1_SEEN;
            else {
                // This is not too scary, it just means we started listening in
                // the middle of a transmission.
                ignoreCount++;
                // if (DEBUG_PRINT) {
                //     Serial.print("(");
                //     Serial.print(ignoreCount);
                //     Serial.print(") Ignoring byte 0x");
                //     Serial.print(myByte);
                //     Serial.print(", have not seen magic word...\n");
                // }
            }
            break;

        case MAGIC_WORD_1_SEEN:
            if (myByte == 0x5A) state = MAGIC_WORD_2_SEEN;
            else if (initialization) {
                // Again, this is just us listening for the correct combination
                // when first starting up.
                state = INITIAL;
            } else {
                // TODO: handle missing second word
                if (DEBUG_PRINT) Serial.print("Magic word 2 missing! Go to initial...\n");
                state = INITIAL;
            }
            break;

        case MAGIC_WORD_2_SEEN:
            initialization = false;
            // if (DEBUG_PRINT) printf("Saw both magic words!\n");
            frame->messageId = myByte;
            state = MESSAGE_ID_SEEN;
            break;

        case MESSAGE_ID_SEEN:
            frame->messageClass = myByte;
            state = CLASS_SEEN;
            dataLengthPosition = 0; // Ensure we start at zero position for
                                    // reading the data length
            break;

        case CLASS_SEEN:
            if (dataLengthPosition == 0) {
                // frame->dataLength = myByte;
                frameDataLength = myByte;
                dataLengthPosition = 1;
            } else {
                // THEN dataLengthPosition == 1
                // Little endian, the second byte seen is the larger one
                // frame->dataLength = frame->dataLength | (((int) myByte) << 8);
                frameDataLength = frameDataLength | (((int) myByte) << 8); // arduino
                frame->dataLength = frameDataLength;
                // Serial.print("Data length: ");
                // Serial.println(frame->dataLength);
                state = DATA_LENGTH_SEEN;
                dataPosition = 0; // Ensure we start at zero position in data
            }
            break;

        case DATA_LENGTH_SEEN:
            // Serial.print("DataPos: ");
            // Serial.println(dataPosition);
            // Serial.print("state storage: ");
            // Serial.println((int) &state);

            // Serial.print("frame data storage: ");
            // Serial.print("data pointer: ");
            // Serial.println((int) &(frame->data));
            // Serial.print("dataPosition: ");
            // Serial.println(dataPosition);
            // Serial.print("myByte: ");
            // Serial.println(myByte);
            // Serial.print("About to write to memory...");

            // int* dataPtr = frame->data;
            // frame->data[dataPosition] = myByte;
            frame->data[dataPosition] = myByte;
            // Serial.print("data write ");
            // Serial.print(dataPosition);
            // Serial.print(" of ");
            // Serial.println(frameDataLength);
            if (frameDataLength > 100) {
                Serial.print("Bad frame data length: ");
                Serial.print(frameDataLength);
                Serial.println(" ...returning to initial...");
                state = INITIAL;
            }
            if (dataPosition < (frameDataLength - 1)) {
                dataPosition++;
            } else {
                state = DATA_DONE;
                crcPosition = 0; // Ensure we start at zero for reading the CRC
            }
            break;

        case DATA_DONE:
            if (crcPosition == 0) {
                frameCRC = myByte;
                crcPosition = 1;
            } else {
                // crcPosition = 1
                // Little endian, the second byte seen is the larger one
                frameCRC = frameCRC | (((int) myByte) << 8);
                frame->crc = frameCRC;
                // Serial.println("Read CRC");
                state = CRC_SEEN;
            }
            break;

        case CRC_SEEN:
            if (myByte != 0x33) { // NOTE: Decimal 51
                // TODO: Somehow handle if the ETX is missing
                if (DEBUG_PRINT) Serial.print("Missing ETX! (BAD!) Skipping frame, go to initial state...\n");
                Serial.print("The frame data length was ");
                Serial.println(frameDataLength);
                Serial.print("The value we got was ");
                Serial.println(myByte);
                state = INITIAL;
            } else {
                state = ETX_SEEN;
            }
            break;
        case ETX_SEEN:
            if (myByte == 0xFF) {
                state = MAGIC_WORD_1_SEEN;
            } else {
                // TODO: Handle missing start byte
                if (DEBUG_PRINT) Serial.print(
                    "Missing start byte! This is not good.\n");
            } 
            break;

        
        default:
            Serial.println("UNKNOWN CASE! This should not happen.");
            return 0;

    }

    if (state == ETX_SEEN) return SBG_PARSE_DONE;

    return SBG_PARSE_IN_PROGRESS;

}